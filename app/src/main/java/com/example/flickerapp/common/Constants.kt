package com.example.flickerapp.common

object Constants {
    const val BASE_URL = "https://www.flickr.com/services/rest/"
    const val BASE_IMAGE_URL = "https://live.staticflickr.com/"
}
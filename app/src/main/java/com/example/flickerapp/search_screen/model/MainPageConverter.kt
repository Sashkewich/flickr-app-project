package com.example.flickerapp.search_screen.model

import com.example.flickerapp.search_screen.api.model.FlickrModelResponse
import com.example.flickerapp.search_screen.api.model.PhotoResponse
import com.example.flickerapp.search_screen.api.model.PhotosResponse

object MainPageConverter {
    fun fromNetwork(response: FlickrModelResponse): FlickrModel =
        FlickrModel(
            photos = convertPhotos(response.photos),
        )

    private fun convertPhotos(photosResponse: PhotosResponse): Photos =
        Photos(
            page = photosResponse.page,
            pages = photosResponse.pages,
            perpage = photosResponse.perpage,
            photo = convertPhotoList(photosResponse.photo),
            total = photosResponse.total,
        )

    private fun convertPhotoList(list: List<PhotoResponse>): List<Photo> =
        list.map { data ->
            Photo(
                farm = data.farm,
                id = data.id,
                isfamily = data.isfamily,
                isfriend = data.isfriend,
                ispublic = data.ispublic,
                owner = data.owner,
                server = data.server,
                secret = data.secret,
                title = data.title
            )
        }

}

package com.example.flickerapp.search_screen.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.flickerapp.R
import com.example.flickerapp.common.BaseFragment
import com.example.flickerapp.databinding.FragmentSearchScreenBinding
import com.example.flickerapp.image_screen.ui.ImageFragment
import com.example.flickerapp.search_screen.ui.adapter.HistorySearchAdapter
import com.example.flickerapp.search_screen.ui.adapter.SearchScreenAdapter
import com.example.flickerapp.search_screen.ui.viewmodel.SearchScreenViewModel
import com.example.flickerapp.utils.extensions.replace
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchScreenFragment : BaseFragment(R.layout.fragment_search_screen) {
    private var _binding: FragmentSearchScreenBinding? = null
    private val binding get() = _binding!!
    private val viewModel: SearchScreenViewModel by viewModels()

    // Переменные для работы с историей поиска
    private lateinit var historyAdapter: HistorySearchAdapter
    private val searchHistory = mutableListOf<String>()
    private lateinit var sharedPreferences: SharedPreferences

    private val adapter: SearchScreenAdapter by lazy {
        SearchScreenAdapter { item ->
            val imageFragment = ImageFragment()
            val args = Bundle()
            args.putString("server", item.server)
            args.putString("id", item.id)
            args.putString("secret", item.secret)
            imageFragment.arguments = args
            replace(imageFragment)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchScreenBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentSearchScreenBinding.bind(view)
    }

    override fun bind() {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    activity?.finish()
                }
            }
        )

        sharedPreferences =
            requireContext().getSharedPreferences("SearchHistoryPrefs", Context.MODE_PRIVATE)

        historyAdapter = HistorySearchAdapter(
            searchHistory,
            onItemClick = { query ->
                binding.searchView.setQuery(query, false)
                showImages(adapter, query)
                hideCancelSearchButton()
                hideKeyboard()
            },

            onDeleteClick = { position ->
                removeSearchHistoryItem(position)
            }
        )

        binding.searchHistoryRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.searchHistoryRecyclerView.adapter = historyAdapter
        loadSearchHistory()
    }

    @SuppressLint("DiscouragedApi")
    override fun initViews(view: View) {
        with(binding) {
            imagesRecyclerView.adapter = adapter
            imagesRecyclerView.layoutManager = GridLayoutManager(context, 2)
            imagesRecyclerView.setHasFixedSize(true)
            adapter.onAttachedToRecyclerView(imagesRecyclerView)
            searchView.queryHint = resources.getString(R.string.search_text)
            setupSearchView()
        }
    }

    @SuppressLint("ResourceType")
    private fun showImages(adapter: SearchScreenAdapter, query: String) {
        with(binding) {
            searchHistoryRecyclerView.visibility = View.GONE
            imagesRecyclerView.visibility = View.VISIBLE
        }

        with(viewModel) {
            loadInfoBySearch(
                resources.getString(R.string.api_key),
                query,
                resources.getString(R.string.format),
                resources.getString(R.string.nojsoncallback).toInt(),
                resources.getString(R.string.per_page).toInt(),
                resources.getString(R.string.page).toInt()
            )

            observe(flickrModelStateFlow) { data ->
                adapter.setPhoto(data.photos.photo)
            }
        }
    }

    private fun setupSearchView() {
        with(binding) {
            searchView.setOnQueryTextFocusChangeListener { _, hasFocus ->
                if (hasFocus) { // Если поле поиска находится в действии (в фокусе)
                    imagesRecyclerView.visibility = View.GONE
                    searchHistoryRecyclerView.visibility = View.VISIBLE
                    showCancelSearchButton() // Появляется кнопка "Отмена поиска"

                    binding.cancelButton.setOnClickListener {// Тап на кнопку "Cancel"
                        hideCancelSearchButton() // Скрывается кнопка "Отмена поиска"
                        searchView.clearFocus() // Фокус с поиска исчезает
                    }

                } else { // Если фокус с поля поиска исчезает
                    imagesRecyclerView.visibility = View.VISIBLE // RecyclerView с изображениями появляется
                    searchHistoryRecyclerView.visibility =
                        View.GONE // RecyclerView с историей поиска исчезает
                }
            }

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                @SuppressLint("NotifyDataSetChanged")
                override fun onQueryTextSubmit(query: String?): Boolean {
                    query?.let {
                        if (!searchHistory.contains(it)) {
                            searchHistory.add(0, it)
                            historyAdapter.notifyDataSetChanged()
                            saveSearchHistory()
                        }

                        showImages(adapter, query)
                        hideKeyboard()
                        hideCancelSearchButton()
                    }

                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return true
                }
            })
        }
    }

    private fun hideCancelSearchButton() { // Метод по скрытию кнопки "Cancel" (Отмена поиска)
        binding.cancelButton.visibility = View.GONE
    }

    private fun showCancelSearchButton() { // Метод по появлению кнопки "Cancel" (Отмена поиска)
        binding.cancelButton.visibility = View.VISIBLE
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun loadSearchHistory() { // Метод загрузки истории поиска
        val savedHistory = sharedPreferences.getStringSet("search_history", setOf()) ?: setOf()
        searchHistory.clear()
        searchHistory.addAll(savedHistory)
        historyAdapter.notifyDataSetChanged()
    }

    private fun saveSearchHistory() { // Метод сохранения истории поиска
        with(sharedPreferences.edit()) {
            putStringSet("search_history", searchHistory.toSet())
            apply()
        }
    }

    private fun removeSearchHistoryItem(position: Int) {
        searchHistory.removeAt(position)
        historyAdapter.notifyItemRemoved(position)
        saveSearchHistory()
    }

    private fun hideKeyboard() { // Метод скрытия экранной клавиатуры
        val view = activity?.currentFocus

        view?.let {
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
            binding.searchView.clearFocus()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
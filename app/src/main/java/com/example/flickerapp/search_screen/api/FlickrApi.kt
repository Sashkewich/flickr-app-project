package com.example.flickerapp.search_screen.api

import com.example.flickerapp.search_screen.api.model.FlickrModelResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface FlickrApi {
    @GET("?method=flickr.photos.search")
    suspend fun getInfoBySearch(
        @Query("api_key") apiKey: String,
        @Query("tags") tags: String,
        @Query("format") format: String,
        @Query("nojsoncallback") nojsoncallback: Int,
        @Query("per_page") perPage: Int,
        @Query("page") page: Int
    ) : FlickrModelResponse
}
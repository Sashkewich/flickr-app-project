package com.example.flickerapp.search_screen.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory
import com.example.flickerapp.common.Constants
import com.example.flickerapp.databinding.ImageItemBinding
import com.example.flickerapp.search_screen.model.Photo

class SearchScreenViewHolder(
    private val binding: ImageItemBinding,
    private val clickOnImage: (Photo) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    constructor(
        parent: ViewGroup,
        clickOnImage: (Photo) -> Unit
    ) : this(
        ImageItemBinding.inflate(LayoutInflater.from(parent.context), parent, false),
        clickOnImage
    )

    @SuppressLint("SetTextI18n", "CheckResult", "ResourceType")
    fun onBind(item: Photo) {
        val server = item.server
        val id = item.id
        val secret = item.secret
        val underscore = "_"

        val crossFadeFactory = DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build()

        with(binding) {
            shimmerLayout.startShimmer()
            Glide.with(itemView)
                .load("${Constants.BASE_IMAGE_URL}$server/$id$underscore$secret.jpg")
                .transition(DrawableTransitionOptions.withCrossFade(crossFadeFactory))
                .into(binding.image)


            shimmerLayout.stopShimmer()
            shimmerLayout.setShimmer(null)
            image.visibility = View.VISIBLE

            image.setOnClickListener {
                clickOnImage(item)
            }
        }
    }
}

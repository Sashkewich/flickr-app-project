package com.example.flickerapp.search_screen.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.flickerapp.R
import com.example.flickerapp.search_screen.model.Photo

class SearchScreenAdapter(
    private val clickOnItem: (Photo) -> Unit
) :
    RecyclerView.Adapter<SearchScreenViewHolder>() {
    private val data = mutableListOf<Photo>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchScreenViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.image_item, parent, false)
        return SearchScreenViewHolder(parent, clickOnItem)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: SearchScreenViewHolder, position: Int) {
        val listItem = data[position]
        holder.onBind(listItem)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setPhoto(items: List<Photo>) {
        data.clear()
        data.addAll(items)
        notifyDataSetChanged()
    }
}

package com.example.flickerapp.search_screen.repository

import com.example.flickerapp.search_screen.model.FlickrModel

interface SearchScreenRepository {
    suspend fun getInfoBySearch(
        apiKey: String,
        tags: String,
        format: String,
        nojsoncallback: Int,
        perPage: Int,
        page: Int
    ): FlickrModel
}
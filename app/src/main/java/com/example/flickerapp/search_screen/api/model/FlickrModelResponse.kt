package com.example.flickerapp.search_screen.api.model


import com.google.gson.annotations.SerializedName

data class FlickrModelResponse(
    @SerializedName("photos")
    val photos: PhotosResponse
)
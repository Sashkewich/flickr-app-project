package com.example.flickerapp.search_screen.ui.viewmodel

import androidx.lifecycle.viewModelScope
import com.example.flickerapp.common.BaseViewModel
import com.example.flickerapp.search_screen.di.SearchScreenModule.Companion.IoDispatcher
import com.example.flickerapp.search_screen.interactor.SearchScreenInteractor
import com.example.flickerapp.search_screen.model.FlickrModel
import com.example.flickerapp.search_screen.model.Photos
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.cancellation.CancellationException

@HiltViewModel
class SearchScreenViewModel @Inject constructor(
    private val interactor: SearchScreenInteractor,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : BaseViewModel() {
    private val _flickrModelStateFlow = MutableStateFlow(
        FlickrModel(
            Photos(0, 0, 0, emptyList(), 0)
        )
    )

    val flickrModelStateFlow = _flickrModelStateFlow.asStateFlow()

    fun loadInfoBySearch(
        apiKey: String,
        tags: String,
        format: String,
        nojsoncallback: Int,
        perPage: Int,
        page: Int
    ) {
        viewModelScope.launch {
            withContext(ioDispatcher) {
                try {
                    _flickrModelStateFlow.tryEmit(
                        interactor.getInfoBySearch(
                            apiKey,
                            tags,
                            format,
                            nojsoncallback,
                            perPage,
                            page
                        )
                    )
                } catch (e: CancellationException) {
                    Timber.e(e.message)
                } catch (t: Throwable) {
                    Timber.e(t.message)
                }
            }
        }
    }
}
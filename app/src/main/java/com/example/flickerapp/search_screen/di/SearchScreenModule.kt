package com.example.flickerapp.search_screen.di

import com.example.flickerapp.search_screen.api.FlickrApi
import com.example.flickerapp.search_screen.interactor.SearchScreenInteractor
import com.example.flickerapp.search_screen.repository.SearchScreenRepository
import com.example.flickerapp.search_screen.repository.SearchScreenRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class SearchScreenModule {
    @Binds
    @Singleton
    abstract fun bindRemoteRepository(repository: SearchScreenRepositoryImpl): SearchScreenRepository

    companion object {
        @Provides
        @Singleton
        fun provideApi(retrofit: Retrofit): FlickrApi =
            retrofit.create(FlickrApi::class.java)

        @Provides
        @Singleton
        fun provideInteractor(
            repository: SearchScreenRepositoryImpl,
            @IoDispatcher ioDispatcher: CoroutineDispatcher
        ): SearchScreenInteractor =
            SearchScreenInteractor(repository, ioDispatcher)

        @IoDispatcher
        @Provides
        fun provideContextIo(): CoroutineDispatcher = Dispatchers.IO

        @DefaultDispatcher
        @Provides
        fun provideContextDefault(): CoroutineDispatcher = Dispatchers.Default

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class IoDispatcher

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class DefaultDispatcher
    }
}

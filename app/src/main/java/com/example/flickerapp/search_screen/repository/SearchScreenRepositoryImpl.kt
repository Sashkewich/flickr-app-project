package com.example.flickerapp.search_screen.repository

import com.example.flickerapp.search_screen.api.FlickrApi
import com.example.flickerapp.search_screen.di.SearchScreenModule.Companion.IoDispatcher
import com.example.flickerapp.search_screen.model.FlickrModel
import com.example.flickerapp.search_screen.model.MainPageConverter
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SearchScreenRepositoryImpl @Inject constructor(
    private val api: FlickrApi,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : SearchScreenRepository {
    override suspend fun getInfoBySearch(
        apiKey: String,
        tags: String,
        format: String,
        nojsoncallback: Int,
        perPage: Int,
        page: Int
    ): FlickrModel = withContext(ioDispatcher) {
        MainPageConverter.fromNetwork(
            api.getInfoBySearch(
                apiKey,
                tags,
                format,
                nojsoncallback,
                perPage,
                page
            )
        )
    }
}
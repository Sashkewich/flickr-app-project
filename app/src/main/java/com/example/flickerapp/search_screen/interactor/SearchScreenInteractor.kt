package com.example.flickerapp.search_screen.interactor

import com.example.flickerapp.search_screen.di.SearchScreenModule.Companion.DefaultDispatcher
import com.example.flickerapp.search_screen.model.FlickrModel
import com.example.flickerapp.search_screen.repository.SearchScreenRepositoryImpl
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SearchScreenInteractor @Inject constructor(
    private val repository: SearchScreenRepositoryImpl,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) {
    suspend fun getInfoBySearch(
        apiKey: String,
        tags: String,
        format: String,
        nojsoncallback: Int,
        perPage: Int,
        page: Int
    ): FlickrModel =
        withContext(defaultDispatcher) {
            repository.getInfoBySearch(
                apiKey,
                tags,
                format,
                nojsoncallback,
                perPage,
                page
            )
        }
}

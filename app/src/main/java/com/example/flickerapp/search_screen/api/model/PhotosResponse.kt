package com.example.flickerapp.search_screen.api.model


import com.google.gson.annotations.SerializedName

data class PhotosResponse(
    @SerializedName("page")
    val page: Int,
    @SerializedName("pages")
    val pages: Int,
    @SerializedName("perpage")
    val perpage: Int,
    @SerializedName("photo")
    val photo: List<PhotoResponse>,
    @SerializedName("total")
    val total: Int
)
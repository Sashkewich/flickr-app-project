package com.example.flickerapp.search_screen.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class FlickrModel(
    @SerializedName("photos")
    val photos: Photos
) : Parcelable
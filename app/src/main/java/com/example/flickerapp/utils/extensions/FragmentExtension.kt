package com.example.flickerapp.utils.extensions

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.flickerapp.R

fun Fragment.replace(fragment: Fragment) {
    val fragmentManager = requireActivity().supportFragmentManager
    val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
    fragmentTransaction.addToBackStack(null)
        .replace(R.id.fragmentContainer, fragment)
        .commit()
}

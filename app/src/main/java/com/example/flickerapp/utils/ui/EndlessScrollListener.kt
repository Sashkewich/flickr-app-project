package com.example.flickerapp.utils.ui

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import timber.log.Timber

private const val VISIBLE_THRESHOLD = 10
private const val DEFAULT_ITEMS_COUNT = 20

class EndlessScrollListener(
    private val layoutManager: LinearLayoutManager,
    private val loadNextPage: (totalItems: Int, page: Int) -> Unit,
) : RecyclerView.OnScrollListener() {

    private var isLoading = true
    private var totalLoadedItems = 0

    private var currentPage = 0


    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount
        currentPage = layoutManager.itemCount/DEFAULT_ITEMS_COUNT
        val firstVisibleItem = layoutManager.findFirstVisibleItemPosition()

        Timber.i("/*/ onScrolled visibleItemCount -> $visibleItemCount")
        Timber.i("/*/* onScrolled totalItemCount -> $totalItemCount")
        Timber.i("/*/ onScrolled firstVisibleItem -> $firstVisibleItem")
        Timber.i("/*/ onScrolled totalLoadedItems -> $totalLoadedItems")
        Timber.i("/*/* onScrolled currentPage -> $currentPage")


        if (totalItemCount > totalLoadedItems) {
            isLoading = false
            totalLoadedItems = totalItemCount
            return
        }

        val shouldLoadMore =
            totalItemCount - visibleItemCount <= firstVisibleItem + VISIBLE_THRESHOLD && firstVisibleItem != 0
        Timber.i("/*/ onScrolled shouldLoadMore -> $shouldLoadMore isLoading -> $isLoading")

        if (!isLoading && shouldLoadMore) {
            currentPage++
            loadNextPage(totalItemCount, currentPage)
            isLoading = true
        }
    }
    fun setTotalLoadedItems(totalLoadedItems: Int){
        this.totalLoadedItems = totalLoadedItems
    }

    fun reset() {
        Timber.i("*/* reset")
        isLoading = false
        totalLoadedItems = 0
        currentPage = 1
    }
}



package com.example.flickerapp.image_screen.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.flickerapp.R
import com.example.flickerapp.common.BaseFragment
import com.example.flickerapp.common.Constants
import com.example.flickerapp.databinding.FragmentImageScreenBinding
import com.example.flickerapp.databinding.FragmentSearchScreenBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ImageFragment : BaseFragment(R.layout.fragment_image_screen) {
    private var _binding: FragmentImageScreenBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentImageScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentImageScreenBinding.bind(view)
    }

    override fun bind() {}

    override fun initViews(view: View) {
        val server = arguments?.getString("server")
        val id = arguments?.getString("id")
        val secret = arguments?.getString("secret")
        val underscore = "_"

        Glide.with(requireContext())
            .load("${Constants.BASE_IMAGE_URL}$server/$id$underscore$secret.jpg")
            .into(binding.image)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}